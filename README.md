Cowsay using Alpine Linux
=========================

Build it:

```sh
docker build -t dockertutorial/cowsay .
```

Run it:

```sh
docker run --rm dockertutorial/cowsay Hello
```

Credits:
--------

Cowsay implementation in node: https://github.com/piuccio/cowsay


