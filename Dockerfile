FROM node:4.8-alpine
RUN npm install -g cowsay
ENTRYPOINT ["/usr/local/bin/cowsay"]

